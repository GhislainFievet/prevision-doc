#############
Notifications
#############

When you create a use case or make forecasts, notifications are used to track the progress of the training or prediction process.

These are visible in the lower right corner of your browser when events occur:

.. image:: _static/images/notifs_popup.png
   :align: center

The history of these notifications is accessible from the following icon:

.. image:: _static/images/task_bar_notif.png
   :align: center

.. image:: _static/images/task_bar_notif_2.png
   :align: center

The last 1 000 notifications are listed in a table as follows:

.. image:: _static/images/notifs_screen.png
   :align: center

Each box represents a notification: beginning or end of a process. We find there:

* The name of the use case
* If it is a notification of the beginning or end of the process
* The date of issue of the notification
* The status: "success" or "error".

In case of error, an "i" appears on the concerned tile. Hover the cursor over this icon to display the error content in a tooltip.
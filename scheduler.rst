#########
Scheduler
#########

Prevision.io allows you to schedule tasks, based on user-developed R or Python scripts. These scripts can be used, for example, to create models and make predictions.

The scheduler's role is to automate the recurrence of these actions (e. g. making predictions every day, training every month, etc.)

************************
Table of scheduled tasks
************************

The scheduled tasks table allows you to view all the created tasks. You can access the table by clicking on the most up-right button then scheduler:

.. image:: _static/images/user_menu_scheduler.png
   :align: center

.. image:: _static/images/scheduler.png
   :align: center

Creating a task
===============

To create a new task, you must fill information at the top part of the screen, such as:

* The name of the task
* The path to the related script
* The execution environnement among

   * Python 3.6.5
   * R 3.6.1

* The execution command, typically

   * For python `python script.py`
   * For R `Rscript script.R`

* The frequency of execution

Task table
==========

All tasks created will be displayed in the bottom table with the according informations. You'll see:

* The task name
* The execution command
* The created / modified date
* The last execution
* The frequency
* A contextual menu allowing you to

   * Force run the task (will trigger a new execution on it)
   * Delete the task

Remarks:

* Creating the task can take a few minutes. Avoid creating a task that must start almost immediately
* The execution time to be filled in is in UTC (therefore, there will be a 1-hour time difference in winter and a 2-hour difference in summer)
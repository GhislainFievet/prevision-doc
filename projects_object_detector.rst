###############
Object detector
###############

***********
Performance
***********

.. image:: _static/images/overview_od_perf.png
   :align: center

Performance of object detector use case is computed using mAP (mean Average Precision) : https://medium.com/@jonathan_hui/map-mean-average-precision-for-object-detection-45c121a31173

This metric is computed on up to 5 differents models trained on the Data Set.

Please note that the performance spread can be important between first and last models.

**************
Model analysis
**************

.. image:: _static/images/overview_od_model.png
   :align: center

A random sample of images are displayed with:

* In blue the true bounding box, as supplied by the user
* In orange the predicted bounding box on predicted by Prevision.io on cross validation

Only box with a probability > 10% are displayed in orange.

If none are display, that means that there is no significant detection.

**********
Prediction
**********

.. image:: _static/images/overview_od_predict.png
   :align: center

Prediction works like other image Data Set.

Once done, you can directly check results with probability associated with each bounding box by clicking the see predictions link:

.. image:: _static/images/overview_od_predicted.png
   :align: center